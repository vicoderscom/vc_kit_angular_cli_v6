import { AdminRoutingModule } from './admin-routing.module';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [AdminRoutingModule],
  declarations: []
})
export class AdminModule {}
